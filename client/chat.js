var messages = []
var users = []

var userid

var storage = window.localStorage;

$(function () {
    var socket = io();

    socket.on('connect', () => {
        console.log(socket.id);
        userid = socket.id;
        color = storage.getItem('color');
        console.log("stored color: " + color)
        username = storage.getItem('username');
        console.log("stored username: " + username)
        if (!!color){
            socket.emit('new color', color)
        }
        if (!!username){
            socket.emit('username change', username)
        }
        else{
            storage.setItem('username', socket.id);
        }


    });

    socket.on('userlist', function (userslist) {
        users = userslist;
        let userContainer = document.getElementById('users');
        userContainer.innerHTML = "Online Users";
        for (i = 0; i < users.length; i++) {
            if (users[i].id === userid) {
                $('#users').append($('<li>').text(users[i].username + "(You)").css({ 'font-weight': 'bold' }));
            }
            else {
                $('#users').append($('<li>').text(users[i].username));
            }
        }
        renderMessages();

    });
    $('form').submit(function (e) {
        e.preventDefault(); // prevents page reloading
        let colorcommand = RegExp('^\/color #{0,1}([0-9A-Fa-f]{8}|[0-9A-Fa-f]{6})$');
        let namecommand = RegExp('^\/name (.*)$');
        let wrongcommand = RegExp('^\/');
        if (colorcommand.test($('#m').val()) === true) {
            let hex = $('#m').val().match(colorcommand)
            console.log('color command')
            console.log(hex)
            console.log(hex[1]);
            socket.emit('new color', hex[1])
            $('#m').val('');
            displayPlaceholder('Color Changed Successfully')
            storage.setItem('color', hex[1]);
        }
        else if (namecommand.test($('#m').val()) === true) {
            let name = $('#m').val().match(namecommand);
            console.log(name);
            console.log(name[1])
            socket.emit('username change', name[1])
            $('#m').val('');
            storage.setItem('username', name[1]);
        }
        else if (wrongcommand.test($('#m').val()) === true){
            $('#m').val('');
            displayPlaceholder('Incorrect command')
        }
        else {
            msg = {
                text: $('#m').val(),
                userid: userid
            }
            socket.emit('chat message', msg);
            $('#m').val('');
            displayPlaceholder('');
        }
        return false;
    });

    socket.on('chat message', function (msg) {
        messages = msg
        renderMessages();
        window.scrollTo(0, document.body.scrollHeight);
    });

    socket.on('response',function(err){
        displayPlaceholder(err)
    })

    function renderMessages() {
        let msgContainer = document.getElementById('messages');
        msgContainer.innerHTML = "";
        for (i = 0; i < messages.length; i++) {


            let color = "#" + messages[i].color;
            let username = messages[i].username;
            let text = messages[i].text;
            text = text.replace(':)','😁');
            text = text.replace(':(','🙁');
            text = text.replace(':o','😲');
            
            console.log(text);
            if (messages[i].userid === userid) {
                $('#messages').append($('<li>').text(messages[i].time + " " + username + " " + text).css({ 'font-weight': 'bold', 'color': color }));
            }
            else {
                $('#messages').append($('<li>').text(messages[i].time + " " + username + " " + text).css({ 'color': color }));
            }
        }
    }

    function displayPlaceholder(msg) {
        if (!$('#m').val()) {
            console.log(msg)
            $('#m').attr("placeholder", msg);
        }
    }
});