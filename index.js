var express = require('express')
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 10005;

var messages = []
var users = []

app.use(express.static(__dirname + '/client'))

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/client/index.html');
});

io.on('connection', function (socket) {

  console.log('a user connected');
  console.log(socket.id);
  users.push({
    id: socket.id,
    username: socket.id,
    color: '#000000'
  });
  io.emit('userlist', users)
  
  socket.emit('chat message', messages);


  socket.on('chat message', function (msg) {
    let today = new Date();
    let time = today.toLocaleTimeString();
    let color;
    let username;
    for (j = 0; j < users.length; j++) {
      if (users[j].id === socket.id) {
        color = users[j].color;
        username = users[j].username;
        break;
      }
    }
    let message = {
      text: msg.text,
      userid: msg.userid,
      time: time,
      username: username,
      color: color
    }

    if (messages.push(message) > 200) {
      messages.splice(0, 1);
    }
    console.log('message from ' + message.userid + ': ' + message.text);
    console.log('total messages:' + messages.length);
    io.emit('chat message', messages);
  });

  socket.on('disconnect', function () {
    console.log('user disconnected:' + socket.id)
    for (i = 0; i < users.length; i++) {
      if (users[i].id === socket.id) {
        users.splice(i, 1)
      }
    }
  });

  socket.on('new color', function (hex) {
    console.log('new color for ' + socket.id + ": " + hex)
    for (i = 0; i < users.length; i++) {
      if (users[i].id === socket.id) {
        users[i].color = hex
      }
    }
    for (i = 0; i < messages.length; i++) {
      if(messages[i].userid === socket.id){
        messages[i].color = hex;
      }
    }
    io.emit('userlist', users);
    io.emit('chat message', messages);
  });

  socket.on('username change', function (name) {
    let usernameTaken = false;
    for (i = 0; i < users.length; i++) {
      if (users[i].username === name) {
        usernameTaken = true;
      }
    }
    if(usernameTaken){
      socket.emit('response','Your username is taken. Please select another')
    }
    else{
      
      for (i = 0; i < users.length; i++) {
        if (users[i].id === socket.id) {
          users[i].username = name
        }
      }
      for (i = 0; i < messages.length; i++) {
        if(messages[i].userid === socket.id){
          messages[i].username = name;
        }
      }
      console.log('new username for ' + socket.id + ": " + name)
      io.emit('userlist', users);
      io.emit('chat message', messages);
      socket.emit('response','Username Changed Successfully')

    }
  });
  
});

http.listen(port, function () {
  console.log('listening on *:' + port);
});